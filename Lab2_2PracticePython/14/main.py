class MyPowerList:
    def addItem(self, item, itemsList):
        if itemsList == 2:
            items2.append(item)
            return items2
        else:
            items.append(item)
            return items

    def deleteItem(self,itemNumber, itemsList):
        try:
            if itemsList == 2:
                items2.pop(itemNumber)
                return items2
            else:
                items.pop(itemNumber)
                return items
        except IndexError:
            print("The item does not exist")
        return []

    def sortItems(self):
        for itemCompared in range(0,len(items)):
            for itemComparedWith in range(itemCompared+1, len(items)):
                if items[itemCompared] > items[itemComparedWith]:
                    temp = items[itemCompared]
                    items[itemCompared] = items[itemComparedWith]
                    items[itemComparedWith] = temp
        return items

    def prefixMerge(self):
        return items2 + items

    def suffixMerge(self):
        return items + items2

    def safeToFile(self):
        with open('items.txt', 'w') as f:
            for itemFromList in items:
                f.write("%s\n" % itemFromList)

    def readFromFile(self, itemsList):
        f = open('items.txt', 'r')
        itemsFromFile = f.readlines()
        f.close()
        if itemsList == 2:
            for itemFromFile in itemsFromFile:
                items2.append(itemFromFile.replace("\n",""))
            return items2
        else:
            for itemFromFile in itemsFromFile:
                items.append(itemFromFile.replace("\n",""))
            return items


items = []
items2 = []
option = 0
while option != 9:
    print("Introduce:\n"
          "1 to add an item\n"
          "2 to delete one\n"
          "3 Sort items\n"
          "4 Merge the list 2 as prefix to list 1\n"
          "5 Merge the list 2 as suffix to list 1\n"
          "6 Save to text file\n"
          "7 Read from file\n"
          "9 to exit")
    option = int(input())
    if option == 1:
        print("Introduce the number of list (1 or 2)")
        itemsList = int(input())
        if itemsList != 1 and itemsList != 2:
            print("Invalid items list")
        else:
            print("Introduce the item to add")
            item = input()
            print(MyPowerList().addItem(item,itemsList))
    elif option == 2:
        print("Introduce the number of list (1 or 2)")
        itemsList = int(input())
        if itemsList != 1 and itemsList != 2:
            print("Invalid items list")
        else:
            print("Introduce the number of item to delete")
            itemNumber = int(input()) - 1
            print(MyPowerList().deleteItem(itemNumber,itemsList))
    elif option == 3:
        print(MyPowerList().sortItems())
    elif option == 4:
        print(MyPowerList().prefixMerge())
    elif option == 5:
        print(MyPowerList().suffixMerge())
    elif option == 6:
        MyPowerList().safeToFile()
    elif option == 7:
        print("Introduce the number of list (1 or 2)")
        itemsList = int(input())
        if itemsList != 1 and itemsList != 2:
            print("Invalid items list")
        else:
            print(MyPowerList().readFromFile(itemsList))
    elif option == 9:
        break
    else:
        print("Invalid operation")