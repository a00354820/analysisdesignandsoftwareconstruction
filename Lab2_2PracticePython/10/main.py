class MyPowerList:
    def addItem(self, item):
        items.append(item)
        return items

    def deleteItem(self,itemNumber):
        try:
            items.pop(itemNumber)
        except IndexError:
            print("The item does not exist")
        return items


items = []
option = 0
while option != 9:
    print("Introduce:\n"
          "1 to add an item\n"
          "2 to delete one\n"
          "3 to exit")
    option = int(input())
    if option == 1:
        print("Introduce the item to add")
        item = input()
        print(MyPowerList().addItem(item))
    elif option == 2:
        print("Introduce the number of item to delete")
        itemNumber = int(input()) - 1
        print(MyPowerList().deleteItem(itemNumber))
    elif option == 3:
        break
    else:
        print("Invalid operation")