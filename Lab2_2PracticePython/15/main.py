import csv


users = {"": {}}
users.pop("")

class Manager:

    def newRecord(self):
        print("Name: ")
        name = input()
        print("Address: ")
        address = input()
        print("Phone: ")
        phone = input()
        print("Mail: ")
        mail = input()
        users[name] = {"Address": address, "Phone": phone, "Mail": mail}
        return users[name]

    def safeToFile(self):
        with open('users.csv', 'w') as f:
            for user in users:
                f.write("%s,%s,%s,%s\n" % (user, users[user]["Address"], users[user]["Phone"], users[user]["Mail"]))

    def loadFromFile(self):
        with open('users.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            line_count = 0
            for row in csv_reader:
                users[row[0]] = {"Address": row[1], "Phone": row[2], "Mail": row[3]}
                line_count += 1
            print(f'Processed {line_count} lines.')
        return users

    def searchRecord(self):
        valueToFind = input('Introduce the name of the person to find\n')
        for us in users:
            if us.upper() == valueToFind.upper():
                return users[us]


option = 0
while option != 9:
    print("Introduce:\n" 
          "1 Crete new record\n"
          "2 Save all records in a file\n"
          "3 Load records from a file\n"
          "4 Search and get data from a given record\n"
          "9 to exit")
    option = int(input())
    if option == 1:
        print(Manager().newRecord())
        print(users)
    elif option == 2:
        Manager().safeToFile()
    elif option == 3:
        print(Manager().loadFromFile())
    elif option == 4:
        print(Manager().searchRecord())
    elif option !=9:
        print("Invalid option")