class MyPowerList:
    def addItem(self, item):
        items.append(item)
        return items

    def deleteItem(self,itemNumber):
        try:
            items.pop(itemNumber)
        except IndexError:
            print("The item does not exist")
        return items

    def sortItems(self):
        for itemCompared in range(0,len(items)):
            for itemComparedWith in range(itemCompared+1, len(items)):
                if items[itemCompared] > items[itemComparedWith]:
                    temp = items[itemCompared];
                    items[itemCompared] = items[itemComparedWith];
                    items[itemComparedWith] = temp;
        return items


items = []
option = 0
while option != 9:
    print("Introduce:\n"
          "1 to add an item\n"
          "2 to delete one\n"
          "3 Sort items\n"
          "9 to exit")
    option = int(input())
    if option == 1:
        print("Introduce the item to add")
        item = input()
        print(MyPowerList().addItem(item))
    elif option == 2:
        print("Introduce the number of item to delete")
        itemNumber = int(input()) - 1
        print(MyPowerList().deleteItem(itemNumber))
    elif option == 3:
        print(MyPowerList().sortItems())
    elif option == 9:
        break
    else:
        print("Invalid operation")