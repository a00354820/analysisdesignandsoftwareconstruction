import unittest, math


class TestCeilFunction(unittest.TestCase):

    def test_success(self):
        self.assertEqual(math.ceil(1.1),2)

    def test_verySmallNumber(self):
        self.assertEqual(math.ceil(10**(-19)),1)

    def test_bigNumber(self):
        self.assertEqual(math.ceil(10**19),10000000000000000000)

    def test_inverse(self):
        num = 1.1
        
        inv = math.trunc(num)
        self.assertEqual(math.ceil(num),inv+1)


if __name__ == '__main__':
    unittest.main()