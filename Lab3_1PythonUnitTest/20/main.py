import csv

option = 0
users = {}
fieldMail = "Mail"
fieldAge = "Age"
fieldCountry = "Country"


class Manager:
    def newRecord(self, name, mail, age, country):
        users[name] = {"Mail": mail, "Age": age, "Country": country}
        with open('users.csv', 'w') as csv_file:
            for user in users:
                csv_file.write(
                    "%s,%s,%s,%s\n" % (user, users[user][fieldMail], users[user][fieldAge], users[user][fieldCountry]))
        return users[name]

    def deleteRecord(self, name):
        users.pop(name)
        with open('users.csv', 'w') as csv_file:
            for user in users:
                csv_file.write("%s,%s,%s,%s\n" % (user, users[user][fieldMail], users[user][fieldAge], users[user][fieldCountry]))
        return "deleted"

    def lookByEmailAndAge(self, email, age):
        for user in users:
            if users[user][fieldMail] == email and users[user][fieldAge] == age:
                return user, users[user]


if __name__ == '__main__':
    try:
        with open('users.csv', 'r') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                users[row[0]] = {fieldMail: row[1], fieldAge: row[2], fieldCountry: row[3]}
    except IOError:
        ...

    while option != 9:
        print("Introduce:\n" 
              "1 Crete new record\n"
              "2 Delete a record\n"
              "3 Search by email and age\n"
              "9 to exit")
        option = int(input())
        if option == 1:
            name = input("Name: ")
            mail = input("Mail: ")
            age = input("Age: ")
            country = input("Country: ")
            print(Manager().newRecord(name, mail, age, country))
        elif option == 2:
            name = input("Introduce name of record to delete: ")
            print(Manager().deleteRecord(name))
        elif option == 3:
            email = input("Introduce email: ")
            age = input("Introduce age: ")
            print(Manager().lookByEmailAndAge(email, age))
        elif option != 9:
            print("Invalid option")