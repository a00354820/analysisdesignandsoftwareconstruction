import unittest
from main import Manager
import main


class TestManager(unittest.TestCase):
    def test_newRecord(self):
        got = Manager().newRecord("Javier","a00354820","24","Spain")
        self.assertEqual({'Mail': 'a00354820', 'Age': '24', 'Country': 'Spain'}, got)

    def test_lookByEmailAndAge(self):
        _ = Manager().newRecord("Javier", "a00354820", "24", "Spain")
        got1, got2 = Manager().lookByEmailAndAge("a00354820","24")
        self.assertEqual('Javier', got1)
        self.assertEqual({'Mail': 'a00354820', 'Age': '24', 'Country': 'Spain'}, got2)

    def test_deleteRecord(self):
        _ = Manager().newRecord("Javier", "a00354820", "24", "Spain")
        got = Manager().deleteRecord("Javier")
        self.assertEqual("deleted", got)

    def test_main(self):
        main


if __name__ == '__main__':
    unittest.main()