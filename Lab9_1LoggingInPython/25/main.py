import manager
from tkinter.filedialog import askopenfilename
import logging

logging.basicConfig(filename='logFile.log',
                    filemode='a',
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%H:%M:%S',
                    level=logging.DEBUG)
logging.info('Program started successfully')
print('root INFO Program started successfully')
option = 0
data = []
if __name__ == '__main__':
    while option != 9:
        print('1. Import file\n'
              '2. Sort items\n'
              '9. Exit')
        option = int(input())
        if option == 1:
            path = askopenfilename()
            data = manager.Manager().set_input_data(path)
            print(data)
        elif option == 2:
            data_sorted = manager.Manager().sort(data)
            print(data_sorted)
        else:
            logging.error('Invalid option selected')
            print("root ERROR Invalid option")
