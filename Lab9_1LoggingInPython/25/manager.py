import csv
import logging

logging.basicConfig(filename='logFile.log',
                    filemode='a',
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%H:%M:%S',
                    level=logging.DEBUG)


class Manager:
    def sort(self, received_data):
        data = received_data
        for _ in range(data.__len__()):
            for item_number in range(data.__len__() - 1):
                if data[item_number] > data[item_number + 1]:
                    temp = data[item_number]
                    data[item_number] = data[item_number + 1]
                    data[item_number + 1] = temp
        logging.info('Data sorted successfully')
        print('root INFO Data sorted successfully')
        return data

    def set_input_data(self, file_path_name):
        data = []
        try:
            with open(file_path_name) as csv_file:
                csv_reader = csv.reader(csv_file, delimiter=',')
                for row in csv_reader:
                    for column in row:
                        data.append(column)
        except UnicodeDecodeError:
            logging.error('Invalid file: Error UnicodeDecodeError df17a1b7-3021-47b0-b00b-696f4b96ae05')
            return "root ERROR Invalid file: Error UnicodeDecodeError df17a1b7-3021-47b0-b00b-696f4b96ae05"
        except FileNotFoundError:
            logging.error('Select a .csv file')
            return "root ERROR Select a .csv file"

        logging.info('File imported successfully')
        print('root INFO File imported successfully')
        return data
