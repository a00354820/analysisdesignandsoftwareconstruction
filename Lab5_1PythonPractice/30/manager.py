import csv
import time
import sys


class Manager:
    def sort(self, received_data):
        start_time = time.time()
        print(start_time)
        data = received_data
        records_to_sort = data.__len__()
        print("%s record to sort" % records_to_sort)
        records_sorted = 0
        for _ in range(data.__len__()):
            records_sorted += 1
            percentage = (records_sorted / records_to_sort * 100)
            print(percentage)
            for item_number in range(data.__len__() - 1):
                if data[item_number] > data[item_number + 1]:
                    try:
                        temp = data[item_number]
                        data[item_number] = data[item_number + 1]
                        data[item_number + 1] = temp
                    except TypeError:
                        ...
        end_time = time.time()
        print(end_time)
        time_consumed = end_time - start_time
        print("%s seconds" % time_consumed)
        return data

    def set_input_data(self, file_path_name):
        data = []
        try:
            with open(file_path_name) as csv_file:
                csv_reader = csv.reader(csv_file, delimiter=',')
                for row in csv_reader:
                    for column in row:
                        if column != "":
                            data.append(column)
        except UnicodeDecodeError:
            return "Invalid file: Error UnicodeDecodeError df17a1b7-3021-47b0-b00b-696f4b96ae05"
        except FileNotFoundError:
            return "Select a .csv file"
        return data

    def set_output_data(self, file_path_name, sorted_data):
        full_path = file_path_name+"/sortedData.csv"
        try:
            with open(full_path, "w") as csv_file:
                for item in sorted_data:
                    csv_file.write("%s\n" % item)
                return "File exported successfully"
        except OSError:
            return "Select a valid directory for the file to be saved"
