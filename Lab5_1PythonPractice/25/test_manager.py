import unittest
from manager import Manager


class TestManager(unittest.TestCase):
    def test_sort(self):
        got = Manager().sort([9, 8, 7, 6, 5, 4, 3, 2, 1, 0])
        self.assertEqual(got, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9])

    def test_set_input_data(self):
        got = Manager().set_input_data("/Users/javierhernandezgallegos/OneDrive - Instituto Tecnologico y de Estudios "
                                       "Superiores de Monterrey/2nd Semester/Software/Lab/OtherFiles/Untitled 2.csv")
        self.assertEqual(got, ["Javier", "Elida", "Mayra", "Javi"])

    def test_set_input_data_invalid_file(self):
        got = Manager().set_input_data("/Users/javierhernandezgallegos/OneDrive - Instituto Tecnologico y de Estudios S"
                                       "uperiores de Monterrey/2nd Semester/Software/Lab/OtherFiles/Untitled 2.numbers")
        self.assertEqual(got, "Invalid file: Error UnicodeDecodeError df17a1b7-3021-47b0-b00b-696f4b96ae05")

    def test_set_input_data_invalid_file_no_file_selected(self):
        got = Manager().set_input_data("")
        self.assertEqual(got, "Select a .csv file")


if __name__ == '__main__':
    unittest.main()