import manager
from tkinter.filedialog import askopenfilename

option = 0
data = []
if __name__ == '__main__':
    while option != 9:
        print('1. Import file\n'
              '2. Sort items\n'
              '9. Exit')
        option = int(input())
        if option == 1:
            path = askopenfilename()
            data = manager.Manager().set_input_data(path)
            print(data)
        elif option == 2:
            data_sorted = manager.Manager().sort(data)
            print(data_sorted)
        else:
            print("Invalid option")
