import manager
import tkinter.filedialog

option = 0
data = []
sorted_data = []
if __name__ == '__main__':
    while option != 9:
        print('1. Import file\n'
              '2. Sort items\n'
              '3. Export sorted information\n'
              '9. Exit')
        option = input()
        try:
            option = int(option)
        except ValueError:
            'Invalid option'
        if option == 1:
            path = tkinter.filedialog.askopenfilename()
            data = manager.Manager().set_input_data(path)
            print(data)
        elif option == 2:
            data_sorted = manager.Manager().sort(data)
            print(data_sorted)
        elif option == 3:
            folder_selected = tkinter.filedialog.askdirectory()
            got = manager.Manager().set_output_data(folder_selected, data_sorted)
            print(got)
        elif option == 9:
            ...
        else:
            print("Invalid option")
