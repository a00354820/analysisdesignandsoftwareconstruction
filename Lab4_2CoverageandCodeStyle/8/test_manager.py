import unittest
from operations import Manager
data_set = [2, 4, 6]


class TestManager(unittest.TestCase):
    def test_calculate_mean(self):
        got = Manager().calculate_mean(data_set)
        self.assertEqual(got, 4)

    def test_calculate_sample_standard_deviation(self):
        got = Manager().calculate_sample_standard_deviation(data_set)
        self.assertEqual(got, 2)

    def test_calculate_median(self):
        got = Manager().calculate_median(data_set)
        self.assertEqual(got, 4)

    def test_calculate_n_quartile(self):
        got = Manager().calculate_n_quartile(data_set, 1)
        self.assertEqual(got, 3)

    def test_calculate_n_percentile(self):
        got = Manager().calculate_n_percentile(data_set, 1)
        self.assertEqual(got, 2.04)


if __name__ == '__main__':
    unittest.main()