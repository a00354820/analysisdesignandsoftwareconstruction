import statistics
import numpy


class Manager:
    def calculate_mean(self, data_set):
        return statistics.mean(data_set)

    def calculate_sample_standard_deviation(self, data_set):
        return statistics.stdev(data_set)

    def calculate_median(self, data_set):
        return statistics.median(data_set)

    def calculate_n_quartile(self, data_set, quartile_input):
        quartile = int(quartile_input) * (1/4)
        return numpy.quantile(data_set, quartile)

    def calculate_n_percentile(self, data_set, percentile_input):
        percentile = int(percentile_input)
        return numpy.percentile(data_set, percentile)
