from unittest import TestCase
from main import MyPowerList
import unittest

fakeItem = [1]
fakeItem2 = [9]
fakeItem3 = [10]
fakeItem4 = [11]
fakeItem5 = [12]
fakeItem6 = [13]
fakeItemEmpty = []
fakeItemEmpty2 = []
fakeItemsToSort = [8, 7]
fakeItemsSorted = [7, 8]
fakeItemPrefix = [11, 10]
fakeItemSuffix = [12, 13]
fakeReadItems = [1, 3, "e", "f", "g", "h"]
fakeReadItems2 = [1, 5, 4, "e", "f", "g", "h"]


class TestMyPowerList(TestCase):

    def test_addItem(self):
        got = MyPowerList().addItem(1, 1)
        self.assertEqual(got,fakeItem)

    def test_addItem_items_list2(self):
        got = MyPowerList().addItem(1, 2)
        self.assertEqual(got, fakeItem)

    def test_deleteItem(self):
        _ = MyPowerList().addItem(3, 1)
        got = MyPowerList().deleteItem(3, 1)
        self.assertEqual(got, fakeItemEmpty)

    def test_deleteItem_items_list2(self):
        _ = MyPowerList().addItem(4, 2)
        got = MyPowerList().deleteItem(4, 2)
        self.assertEqual(got, fakeItemEmpty2)

    def test_deleteItem_item_does_not_exist(self):
        _ = MyPowerList().addItem(5, 2)
        got = MyPowerList().deleteItem(6, 2)
        self.assertEqual(got, fakeItemEmpty)

    def test_sortItems(self):
        got = MyPowerList().sortItems(fakeItemsToSort)
        self.assertEqual(got, fakeItemsSorted)

    def test_prefixMerge(self):
        got = MyPowerList().prefixMerge(fakeItem4, fakeItem3)
        self.assertEqual(got, fakeItemPrefix)

    def test_suffixMerge(self):
        got = MyPowerList().suffixMerge(fakeItem5, fakeItem6)
        self.assertEqual(got, fakeItemSuffix)

    def test_readFromFile(self):
        got = MyPowerList().readFromFile(1)
        self.assertEqual(got, fakeReadItems)

    def test_readFromFile_list_2(self):
        got = MyPowerList().readFromFile(2)
        self.assertEqual(got, fakeReadItems2)

    def test_safeToFile(self):
        _ = MyPowerList().safeToFile()

if __name__ == '__main__':
    unittest.main()