import unittest
from main import Manager


class TestManager(unittest.TestCase):
    def test_newRecord(self):
        got = Manager().newRecord("Javier", "Clouthier", "33 3333 3333", "a00354820@itesm.mx")
        self.assertEqual(got, {'Address': 'Clouthier', 'Phone': '33 3333 3333', 'Mail': 'a00354820@itesm.mx'})

    def test_safeToFile(self):
        _ = Manager().newRecord("Javier", "Clouthier", "33 3333 3333", "a00354820@itesm.mx")
        got = Manager().safeToFile()
        self.assertEqual(got, {'Javier': {'Address': 'Clouthier', 'Mail': 'a00354820@itesm.mx',
                                          'Phone': '33 3333 3333'}})

    def test_loadFromFile(self):
        got = Manager().loadFromFile()
        self.assertEqual(got, {'Javier': {'Address': 'Clouthier', 'Phone': '33 3333 3333',
                                          'Mail': 'a00354820@itesm.mx'}})

    def test_searchRecord(self):
        got =Manager().searchRecord("Javier")
        self.assertEqual(got, {'Address': 'Clouthier', 'Phone': '33 3333 3333', 'Mail': 'a00354820@itesm.mx'})


if __name__ == '__main__':
    unittest.main()