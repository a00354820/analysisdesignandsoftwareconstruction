import csv


users = {}


class Manager:

    def newRecord(self, name, address, phone, mail):
        users[name] = {"Address": address, "Phone": phone, "Mail": mail}
        return users[name]

    def safeToFile(self):
        with open('users.csv', 'w') as f:
            for user in users:
                f.write("%s,%s,%s,%s\n" % (user, users[user]["Address"], users[user]["Phone"], users[user]["Mail"]))
        return users

    def loadFromFile(self):
        with open('users.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            line_count = 0
            for row in csv_reader:
                users[row[0]] = {"Address": row[1], "Phone": row[2], "Mail": row[3]}
                line_count += 1
            print(f'Processed {line_count} lines.')
        return users

    def searchRecord(self, valueToFind):
        for us in users:
            if us.upper() == valueToFind.upper():
                return users[us]


