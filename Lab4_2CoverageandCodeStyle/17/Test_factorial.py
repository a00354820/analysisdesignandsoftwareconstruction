import unittest
import math


class TestFactorialFunction(unittest.TestCase):

    def test_SuccessFactorial(self):
        self.assertEqual(math.factorial(1),1)

    def test_FailStr(self):
        self.assertRaises(TypeError,math.factorial,"a")

    def test_FailDecimals(self):
        self.assertRaises(ValueError, math.factorial,1.1)

    def test_FailOverflow(self):
        self.assertRaises(OverflowError,math.factorial,10**19)


if __name__ == '__main__':
    unittest.main()