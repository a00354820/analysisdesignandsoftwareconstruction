import unittest
from main import py_solution

_py_solution = py_solution


class TestPy_solution(unittest.TestCase):
    def test_integer_to_Roman(self):
        got = _py_solution().integer_to_Roman(1)
        self.assertEqual(got, "I")


if __name__ == '__main__':
    unittest.main()